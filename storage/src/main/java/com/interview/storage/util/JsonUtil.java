package com.interview.storage.util;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author on 09.04.19
 */
public class JsonUtil {

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
