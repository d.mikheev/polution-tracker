package com.interview.storage.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Aggregates entity
 *
 * @author 09.04.19
 */
@JsonSerialize
@JsonDeserialize
public class SensorAggregates extends UidEntity implements Serializable {

    private List<DayAggregate> dayAggregates;

    public SensorAggregates() {
    }

    public SensorAggregates(String uid, List<DayAggregate> dayAggregates) {
        super(uid);
        this.dayAggregates = dayAggregates;
    }

    public SensorAggregates(String uid, int qty, double avg, int max, String saveDate) {
        super(uid);
        this.dayAggregates = new ArrayList<>() {{
            add(new DayAggregate(saveDate, avg, max, qty));
        }};
    }

    public List<DayAggregate> getDayAggregates() {
        return dayAggregates;
    }

    public void setDayAggregates(List<DayAggregate> dayAggregates) {
        this.dayAggregates = dayAggregates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorAggregates that = (SensorAggregates) o;
        return Objects.equals(this.getUid(), that.getUid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getUid());
    }
}
