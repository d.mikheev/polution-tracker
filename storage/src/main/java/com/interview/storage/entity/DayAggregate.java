package com.interview.storage.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Objects;

/**
 * @author 09.04.19
 */

@JsonSerialize
@JsonDeserialize
public class DayAggregate {
    private String dayDate;
    private Double avg;
    private Integer max;
    private Integer qty;

    public DayAggregate(String day, Double avg, Integer max, Integer qty) {
        this.dayDate = day;
        this.avg = avg;
        this.max = max;
        this.qty = qty;
    }

    public String getDayDate() {
        return dayDate;
    }

    public void setDayDate(String dayDate) {
        this.dayDate = dayDate;
    }

    public Double getAvg() {
        return avg;
    }

    public void setAvg(Double avg) {
        this.avg = avg;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DayAggregate that = (DayAggregate) o;
        return Objects.equals(dayDate, that.dayDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dayDate);
    }
}
