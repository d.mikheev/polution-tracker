package com.interview.storage.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Entity wrapper
 *
 * @author 09.04.19
 */
@JsonSerialize
@JsonDeserialize
public class UidEntity {
    private String uid;

    public UidEntity() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public UidEntity(String uid) {
        this.uid = uid;
    }
}
