package com.interview.storage.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

/**
 * Alert entity
 *
 * @author on 08.04.19
 */
@JsonSerialize
public class Alert extends UidEntity implements Serializable {

    private String date;
    private Integer measurement;

    public Alert(String uid, Integer measurement, String date) {
        super(uid);
        this.date = date;
        this.measurement = measurement;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Integer measurement) {
        this.measurement = measurement;
    }
}
