package com.interview.storage.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interview.storage.entity.Alert;
import com.interview.storage.entity.SensorAggregates;
import com.interview.storage.entity.UidEntity;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.ReplaceOptions;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.interview.storage.util.JsonUtil.asJsonString;
import static com.mongodb.client.model.Filters.eq;

/**
 * MongoDB repository
 *
 * @author on 06.04.19
 */
@Repository
public class AlertsRepository {

    private final static int ALERT_COLLECTIONS_QTY = 64;

    @Autowired
    private MongoDatabase mongo;

    public void createCollection(String collectionName) {
        mongo.createCollection(collectionName);
    }

    /**
     * Save entity to different collections
     *
     * @param uidEntity entity wrapper
     */
    public void saveUidEntity(UidEntity uidEntity) {
        if (uidEntity instanceof Alert)
            saveAlert((Alert) uidEntity);
        if (uidEntity instanceof SensorAggregates)
            saveAggregates((SensorAggregates) uidEntity);
    }

    /**
     * Save Alert entity to db
     *
     * @param alert Alert entity
     */
    private void saveAlert(Alert alert) {
        String collectionPostFix = String.valueOf(alert.getUid().hashCode() % ALERT_COLLECTIONS_QTY);
        MongoCollection collection = mongo.getCollection("Alerts" + collectionPostFix);
        if (collection == null)
            createCollection("Alerts" + collectionPostFix);
        collection.insertOne(Document.parse(asJsonString(alert)));
    }

    /**
     * Save Aggregates entity to db
     *
     * @param aggregates Aggregates entity
     */
    private void saveAggregates(SensorAggregates aggregates) {
        MongoCollection collection = mongo.getCollection("Aggregates");
        if (collection == null)
            createCollection("Aggregates");
        Document doc = Document.parse(asJsonString(aggregates));
        collection.replaceOne(eq("uid", doc.get("uid")), doc, new ReplaceOptions().upsert(true));
    }

    /**
     * Get sensor aggregates from db
     *
     * @param uid UUID
     * @return SensorAggregates from db if exists
     * @throws IOException
     */
    public SensorAggregates getUidAggregates(String uid) throws IOException {
        MongoCollection collection = mongo.getCollection("Aggregates");
        FindIterable<Document> iterable = collection.find(eq("uid", uid));
        String aggregatesJson = iterable.first().toJson();
        SensorAggregates aggregates = new ObjectMapper().readValue(aggregatesJson, SensorAggregates.class);
        return aggregates;
    }

    /**
     * Get alerts from db
     *
     * @param uid UUID
     * @return list of alerts
     */
    public List<Alert> getAlerts(String uid) {
        String collectionPostFix = String.valueOf(uid.hashCode() % ALERT_COLLECTIONS_QTY);
        MongoCollection collection = mongo.getCollection("Alerts" + collectionPostFix);
        FindIterable<Alert> alerts = collection.find();
        return new ArrayList<>();
    }
}
