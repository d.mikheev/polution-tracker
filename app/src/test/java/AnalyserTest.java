import com.interview.Application;
import com.interview.model.web.SensorMeasurementRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.interview.storage.util.JsonUtil.asJsonString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Functional tests
 *
 * @author 09.04.19
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = Application.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class AnalyserTest {

    @Autowired
    private MockMvc mvc;

    private void sendSensorMeasurement(String uid, List<SensorMeasurementRequest> requests) throws Exception {
        for (SensorMeasurementRequest request : requests)
            mvc.perform(MockMvcRequestBuilders.post("/api/v1/sensors/" + uid + "/mesurements")
                    .content(asJsonString(request))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    @Test
    public void dontExistSensor_whenGetMeasurement_thenStatus209() throws Exception {
        String uid = UUID.randomUUID().toString();
        mvc.perform(get("/api/v1/sensors/" + uid)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void addWarnMeasurements_whenGetMeasurement_thenStatusWarn() throws Exception {
        String uid = UUID.randomUUID().toString();
        List<SensorMeasurementRequest> measurements = new ArrayList() {{
            add(new SensorMeasurementRequest(1000, "2019-02-01T18:55:47+00:00"));
            add(new SensorMeasurementRequest(2000, "2019-02-01T18:56:47+00:00"));
            add(new SensorMeasurementRequest(1000, "2019-02-01T18:57:47+00:00"));
            add(new SensorMeasurementRequest(2100, "2019-02-01T18:58:47+00:00"));
            add(new SensorMeasurementRequest(1000, "2019-02-01T18:59:47+00:00"));
        }};

        sendSensorMeasurement(uid, measurements);

        mvc.perform(get("/api/v1/sensors/" + uid)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is("WARN")));
    }

    @Test
    public void addOkMeasurements_whenGetMeasurement_thenStatusOk() throws Exception {
        String uid = UUID.randomUUID().toString();
        List<SensorMeasurementRequest> measurements = new ArrayList() {{
            add(new SensorMeasurementRequest(2500, "2019-02-01T18:55:47+00:00"));
            add(new SensorMeasurementRequest(2100, "2019-02-01T18:56:47+00:00"));
            add(new SensorMeasurementRequest(1000, "2019-02-01T18:57:47+00:00"));
            add(new SensorMeasurementRequest(1100, "2019-02-01T18:58:47+00:00"));
            add(new SensorMeasurementRequest(1000, "2019-02-01T18:59:47+00:00"));
        }};

        sendSensorMeasurement(uid, measurements);

        mvc.perform(get("/api/v1/sensors/" + uid)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is("OK")));
    }

    @Test
    public void addAlertMeasurements_whenGetMeasurement_thenStatusAlert() throws Exception {
        String uid = UUID.randomUUID().toString();
        List<SensorMeasurementRequest> measurements = new ArrayList() {{
            add(new SensorMeasurementRequest(1000, "2019-02-01T18:55:47+00:00"));
            add(new SensorMeasurementRequest(2000, "2019-02-01T18:56:47+00:00"));
            add(new SensorMeasurementRequest(1000, "2019-02-01T18:57:47+00:00"));
            add(new SensorMeasurementRequest(2100, "2019-02-01T18:58:47+00:00"));
            add(new SensorMeasurementRequest(1000, "2019-02-01T18:59:47+00:00"));
            add(new SensorMeasurementRequest(2500, "2019-02-01T19:00:47+00:00"));
            add(new SensorMeasurementRequest(2100, "2019-02-01T19:01:47+00:00"));
            add(new SensorMeasurementRequest(2600, "2019-02-01T19:02:47+00:00"));
        }};

        sendSensorMeasurement(uid, measurements);

        mvc.perform(get("/api/v1/sensors/" + uid)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is("ALERT")));
    }

    @Test
    public void addMixMeasurements_whenGetMeasurement_thenStatusWarn() throws Exception {
        String uid = UUID.randomUUID().toString();
        List<SensorMeasurementRequest> measurements = new ArrayList() {{
            add(new SensorMeasurementRequest(2500, "2019-02-01T18:55:47+00:00"));
            add(new SensorMeasurementRequest(2100, "2019-02-01T18:56:47+00:00"));
            add(new SensorMeasurementRequest(1000, "2019-02-01T18:57:47+00:00"));
            add(new SensorMeasurementRequest(1100, "2019-02-01T18:58:47+00:00"));
            add(new SensorMeasurementRequest(1000, "2019-02-01T18:59:47+00:00"));
            add(new SensorMeasurementRequest(2500, "2019-02-01T19:00:47+00:00"));
            add(new SensorMeasurementRequest(1100, "2019-02-01T19:01:47+00:00"));
        }};

        sendSensorMeasurement(uid, measurements);

        mvc.perform(get("/api/v1/sensors/" + uid)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is("WARN")));
    }

}
