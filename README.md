# Polution-Tracker-App
### Description
I used Spring Boot and MongoDB. In my solution I implemented concurrent save logic and saved measurements to separate partitions. You can find functional tests on app module in AnalyserTest.class. Unfortunately I did't have enough time to debug /{uuid}/metrics and /{uuid}/alerts methods.
#### API documentation
The API documentation in swagger format can be found under the following URL: http://localhost:8080/swagger-ui.html
Or in case you run the app on different host, you should replace the host name with the one where app is actually running.

#### Modules
- app - Spring Boot settings, swagger and docker building properties
- storage - Mongo DB repository and entities
- tracker - REST API and business logic

#### Before run
- Change host and port of Mongo DB in com.interview.storage.configuration.MongoDBConfig. Current values are host="localhost", port=27017.
- Create database sensors-measurements
```bash
mongo --shell use sensors-measurements
```

### Running instructions.
1. Download or clone the project.
2. Change to the root project directory.
3. Build the project using gradle wrapper:
```bash
./gradlew :app:bootJar
```
On windows use
```bash
gradlew.bat :app:bootJar
```
4. Change to the <project root>/app/build/libs
5. Run the jar:
```bash
  java -jar polution-tracker-app-0.1.0.jar
  ```
If something isn't working or you have any questions, feel free to contact me.