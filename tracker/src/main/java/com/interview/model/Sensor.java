package com.interview.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * Sensor model
 *
 * @author on 07.04.19
 */
public class Sensor implements Serializable {

    private final String uid;
    private double avgCo2;
    private Integer maxCo2;
    private Integer co2;
    private Integer measurementQty;
    private int[] measurementHistory;
    private String lastIterTime;

    public Sensor(String uid, int co2, String lastIterTime) {
        this.uid = uid;
        this.lastIterTime = lastIterTime;
        this.co2 = co2;
        this.avgCo2 = co2;
        this.maxCo2 = co2;
        this.measurementQty = 1;
        this.measurementHistory = new int[]{0, 0, 0};
    }

    public Integer getCo2() {
        return co2;
    }

    public void setCo2(Integer co2) {
        this.co2 = co2;
    }

    public String getUid() {
        return uid;
    }

    public double getAvgCo2() {
        return avgCo2;
    }

    public void setAvgCo2(double avgCo2) {
        this.avgCo2 = avgCo2;
    }

    public Integer getMaxCo2() {
        return maxCo2;
    }

    public void setMaxCo2(Integer maxCo2) {
        this.maxCo2 = maxCo2;
    }

    public Integer getMeasurementQty() {
        return measurementQty;
    }

    public void setMeasurementQty(Integer measurementQty) {
        this.measurementQty = measurementQty;
    }

    public int[] getMeasurementHistory() {
        return measurementHistory;
    }

    public void setMeasurementHistory(int[] measurementHistory) {
        this.measurementHistory = measurementHistory;
    }

    public String getLastIterTime() {
        return lastIterTime;
    }

    public void setLastIterTime(String lastIterTime) {
        this.lastIterTime = lastIterTime;
    }

    public void addMeasurementHistory(int value) {
        this.measurementHistory = new int[]{value, measurementHistory[0], measurementHistory[1]};
    }

    public void incMeasurementQty() {
        this.measurementQty++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sensor)) return false;
        Sensor sensor = (Sensor) o;
        return Objects.equals(getUid(), sensor.getUid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUid());
    }
}
