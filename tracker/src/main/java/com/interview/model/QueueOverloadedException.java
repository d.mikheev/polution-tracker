package com.interview.model;

/**
 * Queue overloaded exception
 *
 * @author 09.04.19
 */
public class QueueOverloadedException extends Exception {
    public QueueOverloadedException(String message) {
        super(message);
    }
}
