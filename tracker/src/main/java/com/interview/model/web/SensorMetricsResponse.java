package com.interview.model.web;

import io.swagger.annotations.ApiModelProperty;

/**
 * Sensor metrics response
 *
 * @author on 07.04.19
 */
public class SensorMetricsResponse {

    private Integer maxLast30Days;
    private Double avgLast30Days;

    public SensorMetricsResponse() {
    }

    public SensorMetricsResponse(Integer maxLast30Days, Double avgLast30Days) {
        this.maxLast30Days = maxLast30Days;
        this.avgLast30Days = avgLast30Days;
    }

    @ApiModelProperty(value = "Maximum from 30 days", example = "2800")
    public Integer getMaxLast30Days() {
        return maxLast30Days;
    }

    public void setMaxLast30Days(Integer maxLast30Days) {
        this.maxLast30Days = maxLast30Days;
    }

    @ApiModelProperty(value = "Sensor avg value", example = "2543.6666")
    public Double getAvgLast30Days() {
        return avgLast30Days;
    }

    public void setAvgLast30Days(Double avgLast30Days) {
        this.avgLast30Days = avgLast30Days;
    }
}
