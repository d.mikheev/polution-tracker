package com.interview.model.web;

import com.interview.model.enums.SensorStatus;
import io.swagger.annotations.ApiModelProperty;

/**
 * Sensor status response
 *
 * @author on 07.04.19
 */
public class SensorStatusResponse {

    private SensorStatus status;

    public SensorStatusResponse(SensorStatus status) {
        this.status = status;
    }

    @ApiModelProperty(value = "Sensor status", example = "WARN")
    public SensorStatus getStatus() {
        return status;
    }

    public void setStatus(SensorStatus status) {
        this.status = status;
    }
}
