package com.interview.model.web;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Sensor measurement request
 *
 * @author on 07.04.19
 */

@JsonSerialize
@JsonDeserialize
@ApiModel(value = "SensorMeasurementRequest", description = "Model of SensorMeasurementRequest")
public class SensorMeasurementRequest implements Serializable {
    private Integer co2;
    private String time;

    public SensorMeasurementRequest() {
    }

    public SensorMeasurementRequest(Integer co2, String time) {
        this.co2 = co2;
        this.time = time;
    }

    @ApiModelProperty(value = "co2 level", example = "2000")
    public Integer getCo2() {
        return co2;
    }

    public void setCo2(Integer co2) {
        this.co2 = co2;
    }

    @ApiModelProperty(value = "Measurement date", example = "2019-02-01T18:55:47+00:00")
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
