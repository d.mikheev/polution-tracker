package com.interview.model.enums;

import java.util.Arrays;

/**
 * Sensor status calculator
 *
 * @author on 07.04.19
 */
public enum SensorStatus {

    OK(0),
    WARN(1),
    ALERT(10);

    private static final int CO2_MAX_LEVEL = 2000;
    private final int value;

    SensorStatus(int value){
        this.value = value;
    }

    /**
     * Sensor status of single measurement
     *
     * @param co2 measurement of co2
     * @return sensor status
     */
    public static SensorStatus getLevelStatus(int co2){
        if (co2>CO2_MAX_LEVEL)
            return WARN;
        return OK;
    }

    /**
     * Check that all statuses in history are ok
     *
     * @param measurementHistory massive of last 3 sensor statuses
     * @return true if all statuses in history equals ok
     */
    private static boolean allIsOk(int[] measurementHistory){
        return Arrays.stream(measurementHistory)
                .allMatch(e->e==OK.getValue());
    }

    /**
     * Check that all statuses in history are warn
     *
     * @param measurementHistory massive of last 3 sensor statuses
     * @return true if all statuses in history equals warn
     */
    private static boolean allIsWarn(int[] measurementHistory){
        return Arrays.stream(measurementHistory)
                .allMatch(e->e==WARN.getValue());
    }

    /**
     * Sensor measurement history status
     *
     * @param measurementHistory massive of last 3 sensor statuses
     * @return sensor status
     */
    public static SensorStatus analyzeHistory(int[] measurementHistory){
        if (allIsOk(measurementHistory))
            return OK;
        if (allIsWarn(measurementHistory))
            return ALERT;
        return WARN;
    }

    public int getValue() {
        return value;
    }
}
