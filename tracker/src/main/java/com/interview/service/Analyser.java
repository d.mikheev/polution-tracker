package com.interview.service;

import com.interview.model.QueueOverloadedException;
import com.interview.model.enums.SensorStatus;

import java.util.NoSuchElementException;

/**
 * Sensor operations interface
 *
 * @author on 08.04.19
 */
public interface Analyser {
    /**
     * Save sensor measurements
     *
     * @param uid UUID
     * @param co2 co2 measurement
     * @param measurementTime time of measurement
     * @throws QueueOverloadedException if inbound queue is full
     */
    public void addMeasurement(String uid, int co2, String measurementTime) throws QueueOverloadedException;

    /**
     * Get sensor status
     *
     * @param uid UUID
     * @return status of sensor {WARN,ALERT,OK}
     * @throws NoSuchElementException if sensor doesn't exist
     */
    public SensorStatus getSensorStatus(String uid) throws NoSuchElementException;

}
