package com.interview.service;

import com.interview.model.QueueOverloadedException;
import com.interview.model.Sensor;
import com.interview.storage.entity.Alert;
import com.interview.storage.entity.SensorAggregates;
import com.interview.storage.entity.UidEntity;
import com.interview.storage.repository.AlertsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Concurrent saving entity manager
 *
 * @author on 08.04.19
 */
@Service
public class Core {

    @Autowired
    @Qualifier("io")
    private final ThreadPoolTaskExecutor executor;
    private static final int QUEUE_MAX_CAPACITY = 10000;
    private ConcurrentLinkedQueue<UidEntity> saveQueue = new ConcurrentLinkedQueue<>();

    @Autowired
    private AlertsRepository repository;

    public Core(@Qualifier("io") ThreadPoolTaskExecutor executor) {
        this.executor = executor;
    }

    @Scheduled(fixedDelay = 1000)
    private void processQueue() {
        while (saveQueue.size() > 0 && executor.getActiveCount() < executor.getMaxPoolSize())
            executor.execute(new SaveTask());
    }

    private void checkQeueIsFull() throws QueueOverloadedException {
        if (saveQueue.size() > QUEUE_MAX_CAPACITY)
            throw new QueueOverloadedException(String.format("Internal queue contains maximum %s values", QUEUE_MAX_CAPACITY));
    }

    /**
     * Add alert entity to save queue
     *
     * @param sensor sensor model
     * @throws QueueOverloadedException if inbound queue if full
     */
    public void addAlertToQueue(Sensor sensor) throws QueueOverloadedException {
        Alert alert = new Alert(sensor.getUid(), sensor.getCo2(), sensor.getLastIterTime());
        checkQeueIsFull();
        saveQueue.add(alert);
    }

    /**
     * Add aggregate entity to save queue
     *
     * @param sensor sensor model
     * @throws QueueOverloadedException if inbound queue if full
     */
    public void addAggregatesToQueue(Sensor sensor) throws QueueOverloadedException {
        SensorAggregates aggregates = new SensorAggregates(
                sensor.getUid(), sensor.getMeasurementQty(), sensor.getAvgCo2(), sensor.getMaxCo2(), sensor.getLastIterTime());
        checkQeueIsFull();
        saveQueue.add(aggregates);
    }

    /**
     * Simple task for concurrently saving entity to db
     */
    private class SaveTask implements Runnable {
        @Override
        public void run() {
            repository.saveUidEntity(saveQueue.poll());
        }
    }

}
