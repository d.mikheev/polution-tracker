package com.interview.service;

import com.interview.model.QueueOverloadedException;
import com.interview.model.Sensor;
import com.interview.model.enums.SensorStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Sensor measurements saving manager
 *
 * @author on 07.04.19
 */
@Service
public class AlertsAnalyser implements Analyser, Serializable {

    private static final int AGGREGATES_SAVING_RATE = 10;

    private ConcurrentHashMap<String, Sensor> alertsMap = new ConcurrentHashMap<>();
    @Autowired
    private Core core;

    /**
     * {@inheritDoc}
     */
    @Override
    public void addMeasurement(String uid, int co2, String timestamp) throws QueueOverloadedException {
        Sensor currentSensor = alertsMap.get(uid);
        if (currentSensor == null) {
            addNewSensor(uid, co2, timestamp);
            return;
        }
        populateSensor(currentSensor, co2, timestamp);
    }

    /**
     * Create new sensor in inbound map
     *
     * @param uid             UUID
     * @param co2             measurement of co2
     * @param measurementTime time of measurement
     */
    private void addNewSensor(String uid, int co2, String measurementTime) {
        Sensor newSensor = createAlert(uid, co2, measurementTime);
        SensorStatus sensorStatus = SensorStatus.getLevelStatus(co2);
        newSensor.addMeasurementHistory(sensorStatus.getValue());
        alertsMap.put(uid, newSensor);
    }

    /**
     * Add new measurement to existed sensor
     *
     * @param target          existed sensor
     * @param co2             measurement of co2
     * @param measurementTime time of measurement
     * @throws QueueOverloadedException if inbound queue if full
     */
    private void populateSensor(Sensor target, int co2, String measurementTime) throws QueueOverloadedException {
        boolean nextDayFlag = calculateNextDayFlag(target.getLastIterTime(), measurementTime);

        SensorStatus sensorStatus = SensorStatus.getLevelStatus(co2);
        target.addMeasurementHistory(sensorStatus.getValue());
        target = calculateAggregates(co2, target);

        SensorStatus status = SensorStatus.analyzeHistory(target.getMeasurementHistory());

        if (status.equals(SensorStatus.ALERT))
            core.addAlertToQueue(target);

        if (nextDayFlag || needToSaveAggregates(target))
            core.addAggregatesToQueue(target);

        target.setLastIterTime(measurementTime);
    }

    /**
     * Add new measurement to sensor aggregates
     *
     * @param co2    measurement of co2
     * @param target existed sensor
     * @return populated sensor
     */
    private Sensor calculateAggregates(int co2, Sensor target) {
        if (target.getMaxCo2() < co2)
            target.setMaxCo2(co2);

        target.setAvgCo2((target.getAvgCo2() * target.getMeasurementQty() + co2) / (target.getMeasurementQty() + 1));

        target.incMeasurementQty();

        return target;
    }

    /**
     * Create sensor object
     *
     * @param uid             UUID
     * @param co2             measurement of co2
     * @param measurementTime time of measurement
     * @return new sensor object
     */
    private Sensor createAlert(String uid, int co2, String measurementTime) {
        return new Sensor(uid, co2, measurementTime);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SensorStatus getSensorStatus(String uid) throws NoSuchElementException {
        Sensor sensor = alertsMap.get(uid);
        if (sensor == null)
            throw new NoSuchElementException(String.format("There are not any measurements from sensor with uid %s", uid));
        return SensorStatus.analyzeHistory(sensor.getMeasurementHistory());
    }


    /**
     * Was last measurement yesterday
     *
     * @param previousMeasurementTime previous measurement time
     * @param currentMeasurementTime  current measurement time
     * @return true if last measurement was yesterday
     */
    private boolean calculateNextDayFlag(String previousMeasurementTime, String currentMeasurementTime) {
        LocalDateTime previous = LocalDateTime.parse(previousMeasurementTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        LocalDateTime current = LocalDateTime.parse(currentMeasurementTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        if (previous.getDayOfYear() < current.getDayOfYear())
            return true;
        return false;
    }

    /**
     * Flag for saving aggregates in AGGREGATES_SAVING_RATE
     *
     * @param target existed sensor
     * @return true if required to save aggregates on current iteration
     */
    private boolean needToSaveAggregates(Sensor target) {
        int remain = target.getMeasurementQty() % AGGREGATES_SAVING_RATE;
        if (remain == 0)
            return true;
        return false;
    }
}
