package com.interview.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Executor config
 */
@Configuration
public class ThreadConfig {

    private final static int THREAD_PULL_SIZE = Runtime.getRuntime().availableProcessors();
    private final static int AWAIT_TERMINATION_SEC = 30;
    private final static int MAX_PULL_SIZE = 100;
    private final static int QUEUE_CAPACITY = 500;


    @Bean
    @Qualifier("io")
    public ThreadPoolTaskExecutor nioTreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setQueueCapacity(QUEUE_CAPACITY);
        executor.setCorePoolSize(THREAD_PULL_SIZE);
        executor.setMaxPoolSize(MAX_PULL_SIZE);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setAwaitTerminationSeconds(AWAIT_TERMINATION_SEC);
        executor.setThreadNamePrefix("io_executor_thread");
        return executor;
    }

}
