package com.interview.controller;

import com.interview.model.QueueOverloadedException;
import com.interview.model.enums.SensorStatus;
import com.interview.model.web.SensorMeasurementRequest;
import com.interview.model.web.SensorMetricsResponse;
import com.interview.model.web.SensorStatusResponse;
import com.interview.service.Analyser;
import com.interview.storage.entity.Alert;
import com.interview.storage.entity.SensorAggregates;
import com.interview.storage.repository.AlertsRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/v1/sensors/")
@Api(value = "Sensors API", description = "Sensors measurements API")
public class TrackerController {

    @Autowired
    private Analyser analyser;
    @Autowired
    private AlertsRepository repository;

    @ApiOperation(value = "Add sensor measurements")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sensor measurements successfully uploaded"),
    })
    @RequestMapping(value = "/{uuid}/mesurements", method = RequestMethod.POST)
    public ResponseEntity addSensorMesurements(@PathVariable String uuid, @RequestBody SensorMeasurementRequest request) throws QueueOverloadedException {
        analyser.addMeasurement(uuid, request.getCo2(), request.getTime());
        return ResponseEntity.ok("OK");
    }

    @ApiOperation(value = "Get sensor measurements")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sensor measurements successfully recieved"),
            @ApiResponse(code = 209, message = "There are not any measurements from sensor with input uid")
    })
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    @ResponseBody
    public SensorStatusResponse getSensorStatus(@PathVariable String uuid) {
        try {
            SensorStatus sensorStatus = analyser.getSensorStatus(uuid);
            return new SensorStatusResponse(sensorStatus);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage(), e);
        }
    }

    @ApiOperation(value = "Get sensor metrics")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sensor metrics successfully recieved"),
            @ApiResponse(code = 209, message = "There are not any metrics from sensor with input uid")
    })
    @RequestMapping(value = "/{uuid}/metrics", method = RequestMethod.GET)
    public SensorMetricsResponse getSensorMetrics(@PathVariable String uuid) throws IOException {
        try {
            SensorAggregates aggregates = repository.getUidAggregates(uuid);
            return new SensorMetricsResponse(aggregates.getDayAggregates().get(0).getMax(), aggregates.getDayAggregates().get(0).getAvg());
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage(), e);
        }
    }

    @ApiOperation(value = "Get sensor alerts")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sensor alerts successfully recieved"),
    })
    @RequestMapping(value = "/{uuid}/alerts", method = RequestMethod.GET)
    public ResponseEntity getAlertsHistory(@PathVariable String uuid) {
        List<Alert> alerts = repository.getAlerts(uuid);
        return ResponseEntity.ok(alerts);
    }

}
