import com.interview.model.enums.SensorStatus;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author on 09.04.19
 */
public class SensorStatusTest {

    @Test
    public final void analyzeHistoryWarnExpected(){
        Assert.assertEquals(SensorStatus.analyzeHistory(new int[]{1,0,0}),SensorStatus.WARN);
        Assert.assertEquals(SensorStatus.analyzeHistory(new int[]{1,0,1}),SensorStatus.WARN);
        Assert.assertEquals(SensorStatus.analyzeHistory(new int[]{1,1,0}),SensorStatus.WARN);
        Assert.assertEquals(SensorStatus.analyzeHistory(new int[]{0,1,1}),SensorStatus.WARN);
        Assert.assertEquals(SensorStatus.analyzeHistory(new int[]{0,1,0}),SensorStatus.WARN);
        Assert.assertEquals(SensorStatus.analyzeHistory(new int[]{1,0,0}),SensorStatus.WARN);    }

    @Test
    public final void analyzeHistoryAlertExpected(){
        Assert.assertEquals(SensorStatus.analyzeHistory(new int[]{1,1,1}),SensorStatus.ALERT);
    }

    @Test
    public final void analyzeHistoryOkExpected(){
        Assert.assertEquals(SensorStatus.analyzeHistory(new int[]{0,0,0}),SensorStatus.OK);
    }


}
